package com.larefri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;

public class LocationTask extends AsyncTask<Void, Void, List<Address>> {

	private Location currentLocation;
	private LocationManager locationManager;
	private double currentLatitude;
	private double currentLongitude;
	private Context context;
	private Activity parent;
	private static List<Address> addresses;
	private static SharedPreferences settings;
    private boolean locationChanged;
	//private String providerFine;
	//private String providerCoarse;
	//public static final int OUT_OF_SERVICE = 0;
	//public static final int TEMPORARILY_UNAVAILABLE = 1;
	//public static final int AVAILABLE = 2;
	private Dialog dialog;
    private LocationListener locationListener = new LocationListener() {
        @Override
        public synchronized void onLocationChanged(Location location) {
            //Log.e("Location",""+location);
            updateLocation(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            try {
                requestLocation();
            }catch (Exception ex){ }
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    private Timer stopTimer = null;


	public LocationTask(Context context, Activity parent) {
		super();
		this.context = context;
		this.parent = parent;
		this.dialog = null;
		settings = parent.getSharedPreferences(MainActivity.PREFS_NAME, 0);
	}

	public static void sharedPreferences(Activity master){
		settings = master.getSharedPreferences(MainActivity.PREFS_NAME, 0);
	}

	public void updateLocation(Location location){
		try{
            //Log.e("Location sent",(location==null)+"");
			this.setCurrentLocation(location);
			this.currentLatitude = this.getCurrentLocation().getLatitude();
			this.currentLongitude = this.getCurrentLocation().getLongitude();
		}catch(Exception doNotCare){ }
	}

	public static List<Address> getAddresses() {
		return addresses;
	}

	public static String getCity(){
		return settings.getString("current_city", "NO_CITY");
	}

	public void setCity(String current_city){
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("current_city", current_city);
		editor.commit();
	}

	public static String getCountry(){
		return settings.getString("current_country", "NO_COUNTRY");
	}

	public void setCountry(String current_country){
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("current_country", current_country);
		editor.commit();
	}

	public static String getRegion(){
		return settings.getString("current_region", "NO_REGION");
	}

	public void setRegion(String current_region){
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("current_region", current_region);
		editor.commit();
	}

	private void setAddresses(){
		int hints = 0;
		Geocoder gcd = new Geocoder(context, Locale.getDefault());
		addresses = new ArrayList<Address>();
		while(hints<5){
			try {
				addresses = gcd.getFromLocation(currentLatitude, currentLongitude,10);
				break;
			} catch (IOException doNotCare) { /*Log.e("Address",doNotCare.getMessage(),doNotCare);*/ }
            hints++;
		}
		if (addresses.size() > 0){
			this.setCity(addresses.get(0).getLocality().toString());
			this.setCountry(addresses.get(0).getCountryName().toString());
			this.setRegion(addresses.get(0).getAdminArea().toString());
		}
	}

	public Location getCurrentLocation() {
		return this.currentLocation;
	}

	public void setCurrentLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}

	public Criteria initCriteria(){
		Criteria criteria = new Criteria();
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(false);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		return criteria;
	}

	@Override
	protected void onPreExecute() {
        //this.lastLocation = this.getCurrentLocation();
		this.locationManager = (LocationManager) parent.getSystemService(Context.LOCATION_SERVICE);

        String provider = locationManager.getBestProvider(initCriteria(),true);

        if(this.getCurrentLocation() == null && provider != null) {
            //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(provider/*LocationManager.NETWORK_PROVIDER*/, 250, 100, locationListener);

            stopTimer = new Timer();
            stopTimer.schedule(new TimerTask() {

                @Override
                public void run() {
                    // stop after 1 minute, regardless of
                    // whether we successfully got the location
                    // or not
                    stopTimer = null;
                }
            }, 1000 * 60);
            //setAddresses();
        }

		/*for(Store fm: MainActivity.getMyFridgeMagnets()){

			if(fm.getLocales().isEmpty() || !(MainActivity.getMyFridgeMagnets().get(0).getLocales().get(0).getRegion().equalsIgnoreCase(LocationTask.getRegion()))){
				fm.downloadLocales();
			}
		}*/
	}

	@Override
	protected List<Address> doInBackground(Void... params) {
        List<String> providers = this.locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = this.getLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        updateLocation(bestLocation);
        try{
			setAddresses();
		}catch (Exception ex){	}
        return getAddresses();
	}

	public Location getLocation(String provider){
		Location location = this.locationManager.getLastKnownLocation(provider);

		if (location==null){
			location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
		}
		return location;
	}

	private boolean isLocationEnabled(){
		//boolean gps_enabled = false;
        boolean network_enabled = false;
		/*try{
			gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}catch(Exception ex){}*/
		try{
			network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		}catch(Exception ex){}

		return /*gps_enabled ||*/ network_enabled;
	}

	private void requestLocation(){
		// custom dialog
		if(dialog==null){
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle(R.string.location_disabled_title);
			builder.setMessage(R.string.location_disabled);

			builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			dialog = builder.create();
			dialog.show();
		}
	}

	@Override
	protected void onPostExecute(List<Address> result) {
        //isLocationEnabled();
        //Log.e("Location is null?", (this.getCurrentLocation() == null) + "");

        if(!isLocationEnabled() && (this.getCurrentLocation()==null) ){
            try {
                requestLocation();
            }catch (Exception ex){

            }
        }
	}
}
